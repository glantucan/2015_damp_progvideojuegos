

## 0.- Tamaños

Necesitamos decidir cual va a ser el tamaño de la zona de juego y de los ladrillos, la paleta y la pelota.
No necesitamos tamaños exactos de pixel todavía, primero vamos a decidir cual es la relación de aspecto (*aspect ratio*)

* 15-16 bloques horizontal
* Tamaño de pantalla de una tablet ~ 1024 / 768 ---> 1.33 (el clásico 4/3)
* 1024/16 = 128
* Si elegimos una relación de aspecto de 3 para los ladrillos el ancho del ladrillo sería de 21.33333... Nos quedamos con 21, no ha porqué ser pejigueros.
* Y si tenemos una pantalla de lata resolución? iPad retina es justo el doble. **Tamaño zona de juego**: 2048 x 1536 px
* Nos quedamos con ladrillos de 256 x 42 px
  Es decir que si caben 16 ladrillos en horizontal en vertical caben 16.

## 1.- Menus
1. Importa el custom package *DefaultMenus.unitypackge* 
1. Crea una escena nueva y guárdala como *Level_01* en la carpeta *Scenes* del proyecto
1. En esta escena crea un botón: *GameObject > UI > Button*
   Esto creara un *gameObject* de tipo *Canvas* y colocará dentro el botón
1. Expande el botón, selecciona el nodo *Text* que hay dentro y en el *inspector* cambia la etiqueta del botón (propiedad *text*) a `Win`.  
1. Deselecciona el botón pinchando en un lugar vacío de la jerarquía y crea un *empty gameObject*. (*GameObject > Create Empty*)
   Renómbralo a *levelManagerHolder*
1. Arrastra el Script *LevelManager* al game object anterior. 
1. Vuelve a seleccionar el botón. Haz scroll en el inspector hasta que veas un panel que pone *OnClick()*.
1. Pulsa el botón ***+*** que hay abajo a la derecha de él.
1. Debajo del primer botón que aparece pulsa el circulito. Busca *LevelManagerHolder* en la ventana que aparece y seleccionalo. Cierra esta ventana(También podrías arrastrarlo desde la jerarquía)
1. Ahora en el selector que dice *No Function* selecciona *LevelManager > LoadLevel()*   
1. En el campo de texto de debajo escribe `Win Screen`.
1. Abre la escena *Start Menu* pulsa el botón play de Unity 
	* Comprueba que pulsando start en el juego pasamos al nivel 1. 
	* Pulsa el botón *Win* y comprueba que carga la escena *Win Screen*. 
	* Pulsa *Play again* y comprueba que volvemos al menú principal.
	* Pulsa *Quit* y comprueba que aparece en la consola el mensaje `Quit requested`

Ya tenemos configurado nuestro sistema de menús básico.


## 2.- Música y Gestor de sonido

1. Necesitamos una música de fondo. Regístrate en www.freesound.org para poder descargarte la que más te guste.
2. Nos vale cualquier formato de los especificados en el [manual de Unity](http://docs.unity3d.com/Manual/AudioFiles.html) (al final de la página)
3. Crea una carpeta en el proyecto que se llame *Sounds*. Arrastra la música que hayas descargado a esta carpeta.
4. Abre la escena *Start Menu*.
5. Crea un game object vacío. 
6. Llamalo *MusicPlayerHolder*
7. Arrastra el archivo de sonido encima de él en la jerarquía (también puedes arrastrarlo al inspector si lo tienes seleccionado).
8. Ésto creará un componente *Audio Source* al game object. 
9. Busca el volumen y bajalo por debajo de 1/3 del volumen original. Esta es la música de fondo.
10. Ejecuta la escena. Deberías escuchar la música ya que por defecto está marcada la casilla *Play on awake* en el componente *Audio Source*.
11. Si pulsas *Start* la música se para. Hemos cambiado de escena y en *Level_01* no existe el game object *MusicPlayerHolder*

### Haciendo un game object persistente
Podemos hacer que este game object no se destruya automáticamente al cambiar de escena, vamos a crear un script en C#.

1. En la carpeta Scripts del proyecto crea un script que se llame *MusicPlayer* (*Botón derecho > Create > C# Script*)
2. haz doble click sobre el script. Se abrirá Visual Studio.
3. Fíjate en que el nombre de la clase que ha creado unity es el mismo que el nombre del archivo del script. Esto es absolutamente necesario. Cuidado al cambiar el nombre del archivo o de la clase.
4. Dentro de la función `Start()` añade la siguiente sentencia:  
   `GameObject.DontDestroyOnLoad(gameObject);`  
5. Díjate en la diferente capitalización de `GameObject` y `gameObject` ¿Por qué crees que se escriben distinto.
6. Borra la función `Update()` porque no la vamos a utilizar.
7. Guarda y vuelve a Unity
8. Arrastra el script al game object *MusicPlayerHolder*.
9. Ahora este game object tiene un componente *MusicPlayer*. Es una instancia de la clase que hemos creado. Esta es una de las formas más habituales de instanciar objetos en Unity

Si ahora ejecutamos la escena *Start Menu* y pulsamos *Start*, la música sigue reproduciéndose. El game object *MusicPlayerHolder* no se ha destruido esta vez.
incluso si pulsamos *Win* continuaremos escuchando la música de fondo.

Sin embargo si pulsamos *Play Again* en esta última pantalla, al volver al menú de entrada se vuelve a crear un *MusicPlayerHolder*, sin que se destriya el anterior.

Generar objetos que no se destruyen al cambiar de escena provoca este problema. Cada vez que volvamos al menú de entrada tendremos un *MusicPlayerHolder* más.

Para evitarlo, usaremos un patrón que se denomina *Singleton* y que es muy práctico para estas situaciones, aunque hay que utilizarlo con cuidado.
Esta técnica que involucra la creación de una propiedad estática en la clase `MusicPlayer` permite asegurarse de que sólo hay una instancia de la misma en cada momento, destroyendo todas las que se creen adicionalmente antes de que se activen.

### Solo una instancia. El patrón *singleton*

1. Abre la clase MusicPlayer.
2. Crea un campo estático privado que se llame `player` del mismo tipo que la clase.
3. En el método `Start()` crea un condicional que compruebe si ya existía una instancia del *Music Player*.
	* Si no, asigna esta instancia al campo estático `player`. Haz que este game object no se destruya al cargar otra escena (comohacíamos antes)
	* Si ya existe, destruye este game object utilizando `GameObject.Destroy(gameObject)`

Ahora no deberían aparecer más *MusicPlayerHolder*s en la escena al volver al menú principal.

El código de la clase `MusicPlayer` debería quedarte más o menos así:

```
using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    static private MusicPlayer player = null;
	// Use this for initialization
	void Start () {
        if (player == null) {
            player = this;
            GameObject.DontDestroyOnLoad(gameObject);
        } else {
            GameObject.Destroy(gameObject);
        }
    }
}
```

> Si  *MusicPlayerHolder* aparece momentaneamente o la música suena duplicada por unos instantes antes de que de tiempo a que se destruya, renombra la función `Start()` a `Awake()`. Veremos más adelante por qué a veces hay que hacer esto.

## 3.- Imágenes y Sprites
Hablando rápido y mal un sprite es un contenedor de una imagen. Pero de momento esta definición nos vale.

### Resolución y pixels por unidad

#### Importando la imagen de fondo.

* Unity trabaja con unidades arbitrarias. 
* Cuando importamos una imagen debemos decidir que cantidad de píxeles de la imagen equivalen a una unidad arbitraria de Unity. 
* Si no hacemos esto con cuidado con la ayuda de una calculadora, nos va a costar mucho trabajo ajustar los tamaños relativos de los sprites que utilicemos.

* Si lo hacemos, nos podemos aprovechar del *snapping* o *iman* de unity a las unidades exactas para colocar los sprites con mayor comodidad.
* Además no nos tendremos que preocupar por escalarlos. Tendrán el tamaño adecuado desde el principio.

Nuestra imagen de fondo tiene 2048 x 1536 px lo que cumple con las proporciones de 4:3 que queríamos para la pantalla.

Habíamos dicho que queríamos que cada ladrillo ocupe una unidad de Unity y además queremos que quepan 16 ladrillos a lo ancho. 2048/16 nos da 128 pixeles por unidad. Eso es lo que pondremos en las opciones de importación de la imagen de fondo antes de arrastrarla a la escena.

Arrastrar ---> se crea un gameObject con un componente sprite renderer.


#### Ajustando la cámara.

Primero en el panel Game ajustamos la relación de aspecto a 4:3

El tamaño (size de la cámara) indica la mitad del número de unidades de unity que abarca la cámara en verical. 

16/4*3 = 12 unidades en total en verical. La mitad -> size = 6 

Ahora colocamos la imagen de fondo el (-8, 6, 5) 



#### Los ladrillos

Creamos la imagen de los ladrillos para que quepan 16 a lo ancho de la escena, cumpliendo con la resolución objetivo. De nuevo 2048/16 nos da 128 pixeles. Queremos que alto/ancho del ladrillo sea 3.
128/3 = 42.666666... Nos quedamos con 43.

Al importar los ladrillos como, queremos que cada uno ocupe una unidad, ponemos los píxeles por unidad a 128.

#### La pelota 
Queremos que ocupe 40x40pixeles. La dibujamos con ese tamaño y ajustamos los  píxeles por unidad a 128 igual que antes, para mantener la misma escala en todas los sprites.


### Prefabs
Vamos a crear muchos sprites de ladrillos iguales, y después vamos a ir añadiéndoles a todos la misma funcionalidad. Esto puede ser un poco tedioso si no utilizamos los prefabs de Unity.
Un prefab es un gameObject prefabricado, es como una clase de la que podemos crear instancias.
Si después añadimos componentes o cambiamos la configuración del prefab, todas sus instancias se actualizan.

Así que nada más arrastrar el primer ladrillo a la escena, creamos una carpeta de proyecto llamada *Prefab* y arrastramso el ladrillo desde la escena a dicha carpeta.

Ahora el ladrillo de la escena es una instancia del prefab. Podemos crear copias de ella y cada una de ellas será una instancia del mismo prefab.

También podemos arrastrar el prefab a la escena para crear una nueva instancia del mismo.

Abrimos Snap settings y ponemos a (0.5, 0.3333333, 1) (0.33333 porque la altura de los ladrillos es 1/3 de unidad)

### Hacer que la pelota se mueva
Añadir Rigidbody2D a la pelota. Comprobar que cae



## 4.- Colliders y Triggers

![](CollidersAndTriggers.png)


### Para que perdamos cuando la bola pasa por el límite inferior de la pantalla
1. Le añadimos un Collider2D circular a la pelota. 
2. Nos aseguramos de que está configurado como Collider (queremos que choque con los ladrillos y la paleta) y de que el collider tiene el mismo tamaño que la imagen de la pelota.
1. Nuevo game object vacío
2. Le añadimos un Collider2D
3. Probamos la diferencia en el comportamiento de la pelota cuando se configura como trigger o se deja como collider
3. Lo configuramos como Trigger
4. Creamos una clase: DieOnTriggerEnter
5. Le añadimos una función OnTriggerEnter2D
6. Primero probamos que funcione poniendo `print("DieOnTrigger Triggered by: " + col.name);` en el interior (Haced siempre esto con los eventos)
6. Cargamos la escena del menu dentro de esa función. 

Comprobamos que volvemos al menú principal cuando perdemos.

### Rebotando en la paleta

Miramos la tabla anterior y vemos que como la pelota tiene un Rigidbody2D nos vale que la paleta sea un collider cinemático (no se mueve obedeciendo a la física, sino al movimiento del ratón)

1. Añadimos un box collider (2D) a la pelota
2. Añadimos también un Rigidbody2D (para que no sea un collider estático)
2. Marcamos la casilla *kinematic* en el componente Rigidbody2D. (Pero antes vemos lo que pasa si no lo hacemos)
3. Comprobamos que la pelota se para en la paleta (la colocamos  debajo de la pelota previamente). 

#### Materiales Físicos
La pelota se para pero no rebota. Para eso necesitamos un *Physic Material*

1. Crea una carpeta *PhysicsMaterials* y crea un *PhysicsMaterial2D* dentro. Llamalo *BounceMaterial*
2. Configura la fricción a 0 y la elasticidad (*Bounciness*) a 1.
3. Selecciona la pelota y arrastra el *BounceMaterial* hasta la propiedad Material del collider.


Seguramente verás que la pelota cada vez rebota más y más alto. Esto ocurre porque el método de detección de colisiones de Unity por defecto es discreto, se comprueban sólo cada cierto tiempo y, en este caso, la colisión es detectada más tarde de lo que debería y gana algo de velocidad extra antes de rebotar cada vez.

No es un bug, simplemente Rigidbody2D está configurado por defecto para detectar colisiones de la forma más barata posible en términos de procesamiento. Esto no produce simulaciones físicamente correctas en algunos casos como este. La forma de resolverlo es cambiar el método de detección de colisiones del Rigidbody2D de la pelota a continuo (*Collision Detection*: *Continuous*)

### Moviendo la paleta
En este apartado vamos a conocer mejor el componente *transform*, y descubrir la clase `Vector3`, y las clases `Input`y `Screen` y `Mathf`.

Para mover la paleta tendremos que cambiar la propiedad *position* de su componente *transform*.
Este componente tiene otras dos propiedades visibles en el inspector, *rotation* y *scale*.
Las tres son de tipo `Vector3`. 

Los tres números que aparecen son coordenadas, en el caso de la posición, ángulos de giro respecto a los ejes, para la rotación, y escalas en cada una de las tres direcciones del espacio.

Consulta la página de referencia de Unity sobre la clase [`Transform`](http://docs.unity3d.com/ScriptReference/Transform.html) (que es la que está detrás del componente del mismo nombre) y la clase [`Vector3`](http://docs.unity3d.com/ScriptReference/Vector3.html).  

Lo que la referencia llama *Variables* son propiedades (suelen ser getters y setters públicos) y lo que llama *public functions* son métodos públicos.
Fíjate en que también tenemos un apartado de *inherited members* y de *Static Functions* cuando los hay.
Dedícale tiempo a investigar los contenidos de cada una de estas clases y leer la documentación. El objetivo es saber lo que pueden hacer por ti, y acordarse cuando lo necesites. No hace falta aprenderse nada de memoria.

Si pinchamos en el enlace de uno de los miembros tenemos documentación adicional, y normalmente algún ejemplo de uso. Hagámoslo con la propiedad `position` de `Transform`. Cómo ves es un `Vector3`, y además del ejemplo tenemos un enlace a la clase que define su tipo. Sigámoslo, porque aquí hay un detalle que suele despistar a los recien llegados a Unity y a C#.

#### Los vectores son valores no referencias a un objeto. Estructuras en C#.
En el segundo párrafo de la documentación de `Vector3` dice que es una *estructura*. ¿Qué significa esto? Las estructuras, que se definen en C# de forma parecida a una clase pero con la palabra clave `struct` crea tipos de tipo valor, como los tipos primitivos. Se almacenan en la *pila* de memoria (*Stack*) en lugar de en el *montón* (*Heap*), para los que sepáis de que va esto.


Pero lo importante es que cuando usamos una variable de un tipo definido por una estructura no recibimos un puntero a un objeto, sino el valor en sí, igual que cuando usamos una variable de tipo `string`, `float` o `bool`. 

Esto implica que no sirve de nada cambiar la componente `x` de transform.position de la siguiente manera:

```
transform.position.x = 3.0f;
```

Porque `position` es un getter que devuelve una copia del vector y estamos cambiando la componente `x` de esa copia. Lo que dejaría el game object en la misma posición en la que estaba.

#### Típico error de principiante. No se pueden cambiar las coordenadas de un gameObject directamente
Todo esto es para explicar por qué el compilador de Unity devuelve un error cuando intentamos hacer esto:

```
error CS1612: Cannot modify a value type return value of `UnityEngine.Transform.position'. 
Consider storing the value in a temporary variable

```
Unity impide que intentemos hacer esto para evitar que los programadores no familiarizados con las estructuras de C# cometan el error de intentar cambiar una propiedad en la copia que nos devuelve `position`, que cambiará efectivamente, pero esa copia no es la que está en el componente Transform y por tanto no cambiaría nada. Esto, si no fuera por este error de compilación introducido por Unity, sería un bug con un origen muy difícil de detectar, con la desesperación que esto conlleva.

Así que lo que hay que hacer para cambiar la posición es crear un nuevo `Vector3`, modificar sus coordenadas y volver a asignar todo el vector a la propiedad `position`. 


Ahora que esto ha quedado claro (hopefully) veamos qué vamos a usar para darle valor a las coordenadas del vector `position` de la paleta.

#### `Input` y  `Screen` 
Echadle un vistazo a la clase [`Input`](http://docs.unity3d.com/ScriptReference/Input.html). Fíjate en que tiene una propiedad estática que se llama `mousePosition` que parece ser justo loque necesitamos.
Antes de utilizarla entra en su descripción y léela.

Intenta utilizar la función `Update()` (que se ejecutará automáticamente en cada actualización de pantalla) de la clase que hemos creado para modificar la posición de la paleta para ajustarse a la coordenada x del ratón. Necesitarás averiguar el ancho en píxeles de la pantalla, puedes utilizar para ello la propiedad estática de [`Screen`](http://docs.unity3d.com/ScriptReference/Screen.html) que necesites.

```
using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newPosition = new Vector3(
            Input.mousePosition.x / Screen.width * 16F - 8F,
            transform.position.y,
            transform.position.z);
        transform.position = newPosition;
        
    }
}

```
#### `Mathf.Clamp()` 
Ahora intenta limitar el movimiento de la paleta a los límites de la pantalla usando el método estático `Clamp()` de [`Mathf`](http://docs.unity3d.com/ScriptReference/Mathf.html).


## 5.- Techo y paredes. Lanzando la pelota al comenzar

### El prefab de la zona de juego
Vamos a crear unos muros de ladrillos para el techo y las paredes laterales que limitan la zona de juego.

1. Si no lo habías hecho ya, abre la ventana de configuración de *Snap Settings*. Pon 0.5 en *Move X* y 0.3333333... en Move Y. Los demás propiedades se pueden quedar como están.
2. Deberías tener un ladrillo en la escena que creamos en un apartado anterior. No le pongas collider todavía, quítaselo si se lo habías puesto antes de hacer lo siguiente.
2. Crea un prefab del ladrillo que arrastramos antes a la escena, si es que no lo has hecho ya. REnómbralo y llámalo *wallBrick*, para diferenciarlo de los de juego que son destruibles.
3. Arrastra el prefab a la escena y colocalo en las coordenadas (-7.5, 6, 0).
   Debería quedar en alineado arriba a la izquierda de la escena. 
4. Ve creando copias del ladrillo (Ctrl+D) y arrantrándolos con la tecla Ctrl pulsada, verás que te resulta muy fácil colocarlos.
2. Crea una fila de 16 ladrillos horizontal para el techo. 
3. Una vez colocados, crea un game object vacío, colocalo en el borde superior izquierdo de la escena (-8, 6, 0) y arrastra todos los ladrillos dentro de él. Renombra dicho grupo a *Ceiling*
4. Selecciona el grupo. Pulsa Ctrl+D para duplicarlo. 
5. Gíralo 90 grados respecto al eje Z
6. Borra los ladrillos sobrantes. Y ajusta la posición del grupo si hace falta.
7. Ahora selecciona todos los ladrillos. Arrastralos fuera del grupo, pero con cuidado de no meterlos dentro de otro game object.
8. Selecciona el game object que ha quedado vacío, *Ceiling (Clone)*. Y asegurate de que su ratocón es (0,0,0) y que su escala es (1,1,1). Si no corregimos esto pasan cosas raras que luego es difícil resolver.
9. Vuelve a meter los ladrillos dentro. Renombra el grupo a *LeftWall*
10. Duplica *LeftWall*, renombralo a *RightWall* y colócalo a la derecha.

**Cuidado al seleccionar, mantén simpre un ojo puesto en la jerarquía para saber qué tienes seleccionado antes de realizar una operación**. Es muy fácil equivocarse y cambiar el objeto equivocado y no darse cuenta hasta mucho tiempo después. A veces esto implica repetir un montón de trabajo.

Vamos a poner colliders en *Ceiling*,  *LeftWall* y *RightWall*. Pero para que sea más fácil colocarlos, crearemos un gameObject vacío dentro de ellos que vamos a llamar, respectivamente, *TopCollider*, *LeftCollider* y *RightCollider*.
Conviene que configuremos su tamaño con las propiedades del componente BoxCollider2D que le añadamos. Intenta no modificar la escala del Trasnform nunca, o casi nunca. En general eso complica las cosas de forma poco predecible. 

No nos gusta lo que no es predecible, normalmente implica bugs cuyo origen es difícil de encontrar.

Los muros no necesitan Rigidbody, no se van a mover.

Todavía vamos a agrupar más los muros. Crea un gema object vacío, llámalo *GameArea* y colócalo en (-8,6,0). Selecciona  *Ceiling*,  *LeftWall*, *RightWall* y el *DieCollider* que creamos en la sección anterior y arrastralos dentro de  *GameArea*.

Ahora arrastra *GameArea* a la carpeta de prefabs para no tener que hacer todo esto en cada nivel.


### Lanzando la pelota para empezar.

Queremos que la pelota esté inicialmente pegada a la paleta hasta que pulsemos el botón izquierdo del ratón.
Para ello necesitamos registrar al inicio la posición de la pelota respecto a la paleta (desde donde la dejaremos colocada al configurar la escena. Más adelante cambiaremos esto y crearemos la pelota desde el programa, pero de momento esto nos vale)
También necesitamos un flag que nos indique si ya se ha disparado la pelota.

1. Crea un script que se llame `BallMovement`
2. Crea un campo público que se llame `paddleTr`, que contendrá una referenca al componente transform de la paleta.
3. Crea un campo privado que se llame `paddleToBallV3`, que contendrá el vector distancia.
3. Crea un campo booleano privado que se llame `ballFree` que nos indicará si se ha disparado la pelota
4. Te hará falta también un campo privado de tipo `Rigidbody2D`, de manera que podamos asignar a su propiedad `velocity` un `Vector2` (Igual que un `Vector3`, pero sólo con coordenadas x e y)

Ahora, conviene enterarse de que en Unity no tenemos acceso a los constructores de las clases que heredan de `MonoBehaviour`. 

Si queremos inicializar las variables tendremos que hacerlo en la función `Start()` que se ejecutará cuando la escena se haya cargado si el gameObject está activo, y si no, lo hará nada más activarse.
Sólo los  tipos primitivos pueden inicializarse al definir los campos. Los de Unity no son accesibles hasta que no se ha cargado la escena.
Por ejemplo para ppoder acceder al componente *rigidbody* de la pelota y poder cambiar la velocidad necesitamos una referencia a él. Para eso lo típico es utilizar el método GetComponent<Tipo del componente>() en en método Start(): Por ejemplo:

```
//...
private Rigidbody2D rb;

void Start () {
    rb = GetComponent<Rigidbody2D>();
	//...
}
```

La función Update() se ejecuta en cada fotograma, así que inicializar cualquier tipo de variable o construir objetos dentro de ella es, en general un error. No conseguiremos lo que queríamos y/o estaremos añadiendo carga al procesador en cada actualización de pantalla.
 
Otra forma de inicializar variables es a través de Drag And Drop en el inspector. Aparecen en el inspector todos los campos que se declaren como públicso (y los que lleven el metatag de `[SerializeField]`). Además, Unity es muy listo y si arrastras un gameObject a una propiedad del inspector que es del tipo de alguno de sus componentes, asociará el componente, no el gameObject.

Elige el método de inicialización más apropiado para cada variable y construye la clase para el componente que controlará el movimiento de la pelota.

Por último, cambia el valor del *Gravity Scale* del Rigidbody de la pelota a 0.1, para que la gravedad no le afecte tanto.


## 6.- Los ladrillos

En esta sección vamos a hacer uso de los prefabs para configurar cada tipo de ladrillo para nuestro prototipo inicial.

### Los prefabs permiten crear niveles reaprovechando lo que ya tenemos
**Practicar Apply y Revert**
Ya teníamos un prefab creado, pero nuestros ladrillos de momento no tienen collider ni enteraccionan con la pelota de ninguna manera.

Vamos ha hacer los cambios necesarios para solucionar esto y añadir un script que controle el número de toques de la bola para que el ladrillo sea destruido y guardar todo esto en el prefab básico del ladrillo.
Ejercicio: añadir el collider al ladrillo y crear un Script: BrickResistance que destruya el ladrillo cuando la bola choque contra él un número de veces predeterminado en el inpector.

Convertiremos todo en un prefab para que podamos reutilizarlo en diferentes niveles.
Crearemos los siguientes prefabs:
* ball
* brick
* LevelManagerHolder
* Main Camera
* paddle
* PlaySpace

Crearemos un nuevo nivel y arrastraremos todos estos prefabs a la escena (borrando previemanete la cámara que se crea por defecto)
Casi. Cualquier elemento que no hayamos añadido al prefab, en nuestro caso, las luces, no aparecerá en la nueva escena.
Pero podemos volver a la escena anterior, guardar esos elementos en los prefab que corresponda (PlaySpace) y al volver a la nueva escena, aparecerán por arte de magia.

Hay un problema, la pelota ha perdido la referencia a la paleta. Esto ha ocurrido al crear el prefab. Unity no guarda las referencias a otros GOs de la escena en los prefabs. Posible solución, asociar como referencia a la paleta el mismo prefab de la palete. WRONG. Parece resolverlo, pero no. La asociación es con el prefab en sí, no con las instancias que creemos. 
La única solución que no implica cambiar el programa es asociar manualmente la paleta de la escena en cada escena. 
Lo mismo ocurrirá con el trigger de muerte de la parte inferior de la pantalla. necesita tener una referencia a la instancia de `Levelmanager` de la escena actual. 
---> Nos va a servir para entender porqué no podemos hacer asociaciones entre componentes de prefabs. (bola - paleta) => Find(), FindObjectOfType()

Vamos a aprovechar también para parametrizar la velocidad inicial de la pelota si no lo habíamos hecho.

Cambiar el fondo: En el Sprite Renderer.

### Haciendo cambios de escena.
LevelManager será el encargado de contar ladrillos. Creemos un campo privado `brickCount` y dos funciones públicas llamadas `addBrick()` y `SubstractBrick()`, ya imagináis lo que hacen

Añadamos un condicional en `substractBrick()` que detecte cuando hemos eliminado todos los ladrillos y cambie al siguiente nivel usando `Application.LoadLevel()` y `Application.LoadedLevel`.
Falta algo para que esto funcione, añadir en *Build Settings* todos los niveles que hayamos creado.

Ahora, para que esto funcione necesitamos que cada ladrillo (`BrickResistance`) tenga una referencia a la instancia de `LevelManager` cargada con el escenario. Usemos el mismo truco que antes y llamemos a `addBrick()` y `SubstractBrick()` desde el script de los ladrillos cuando corresponda.

Comprueba que se cargan los niveles al acabarse los ladrillos.
Uso de `Invoke()` para retrasar la carga y que de tiempo a mostrar un mensaje (De momento en la consola).

Para hacer las comprobaciones un poco más fáciles vamos a añadir un poco de control sobre el rebote de la pelota en la paleta:

### Rompiendo ladrillos

Primero, queremos tener ladrillos indestructibles que estorben. Eso provoca un pequeño problema. Si no se destruyen todos los ladrillos no se puede pasar de nivel:

Ejercicio: Haz que los ladrillos puedan ser irrompibles y después resuelve el problema.


Es necesario que el jugador tenga una pista visual del número de golpes que le quedan a cada ladrillo.
Tendremos que crear imágenes del ladrillo roto para los 3 estados que tenemos (creamos uno de más por si queremos usarlo después) 

#### SpriteSheets
Vamos a aprender a usar SpriteSheets.
Para cada tipo de ladrillo tendremos un array de instancias `Sprite` en el que pondremos los sprites correspondientes y los cargaremos en el `Spriterenderer` la detectar la colisión dependiendo del número de veces que ha sido golpeado el ladrillo.

#### Sonidos
También mola darle al usuario feedback sonoro. vamos a ponerle sonido al rebote. Y además el sonido a reproducir va  a depender del número de golpes que se le ha dado al ladrillo si este es rompible.
Para esto añadiremos a cada ladrillo una propiedad de tipo AucioClips[] (array)
Además tendremos que usar `AudioSource.PlayClipAtPoint(AusdioClip, Point)` ya que en caso de que el ladrillos se destrulla inmediatamente después, el sonido no se reproduciría. También podríamos hacer el ladrillo invisible en este caso y retrasar la destrucción con un `Invoke()`. 