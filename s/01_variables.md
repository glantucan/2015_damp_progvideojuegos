


<!-- .slide: class="center" -->

### Unity
---

# Variables en C**#**

<br>

---
### DEIM - Prog


----

Variables en C#
<!-- .element: class="header" -->

### ¿Qué vamos a aprender?

<br>

* Para qué sirven las variables  
<!-- .element: class="fragment" -->

* Cómo se definen variables en C#  
	
	<!-- .element: class="fragment" -->
	* Tipos de variable y de valor en C# y en Unity  
	<!-- .element: class="fragment" -->
	
* Como se usan las variables
<!-- .element: class="fragment" -->




-----

Variables en C#
<!-- .element: class="header" -->

### ¿Para que sirven las variables?

* Si quieres mostrar la salud o los puntos que lleva el jugador
<!-- .element: class="fragment  " -->

* Si necesitas saber en diferentes momentos del juego la cantidad de munición que le queda a tu arma
<!-- .element: class="fragment  " -->

* Si necesitas saber cual es el nombre del jugador
<!-- .element: class="fragment  " -->

* Si necesitas usar en tu programa cualquier tipo de información que pueda cambiar
<!-- .element: class="fragment  " -->


**NECESITAS UTILIZAR VARIABLES**
<!-- .element: class="fragment  " -->


Son el elemento básico de cualquier programa.   
Guardan información que puede cambiar durante su ejecución. 
<!-- .element: class="fragment  " -->

-----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

#### Definición de variables

Tipo nombreDeVariable;

<!-- .element: class="formula comparison" -->

```cs
// Ejemplos
int bulletsLeft;
string playerName;
GameObject bulletPrefab;
```

<br>
#### Definición y Asignación en la misma línea

Tipo *nombreDeVariable* = Valor;

<!-- .element: class="formula comparison" -->

```cs
// Ejemplos
int bulletsLeft = 100;
string playerName = "Pepito";
GameObject bulletPrefab = nextWeaponBullet;
```

----

<!-- .slide: class="center" -->

Variables en C#
<!-- .element: class="header" -->
### Inciso:
### Tipos de dato y de variable en C**#** 

----

Variables en C#
<!-- .element: class="header" -->

### Tipos básicos o primitivos:

* `int` para enteros

* `float` para números con decimales

* `bool` para booleanos (`true`, `false`)

* `string` para cadenas de texto (`"Hola mundo!"`)

<br>
* `Vector3`, aunque es propio de Unity,  
funciona como tipo primitivo

<!-- .element: class="fragment" -->
<br>

Hay más, pero estos son los que vamos a utilizar en la inmensa mayoría de los casos.
<!-- .element: class="fragment" -->



----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

#### Los números en C**#**
* C# tiene un montón de tipos de número. 
* **+** flexibilidad y eficiencia de memoria.
	* `sbyte`, `byte`, `short`, `ushort`, `int`, `uint`, `long`, `ulong`, `float` y `double`.
* Al mismo tiempo es un lenguaje muy estricto:
<!-- .element: class="fragment" -->
	* Te obliga a especificar el tipo de cada variable.   
	  Si no lo haces &rArr; ERROR 
 	  <!-- .element: class="fragment" -->
	  
	* Y también te obliga a especificar el tipo de valor   
	con un sufijo:
 	  <!-- .element: class="fragment" -->
	  
		* Si es `int`: Sin sufijo.
 		
		<!-- .element: class="fragment inlineBlock" -->
		* Si es `float`: Con sufijo `f` o `F`
 		
		<!-- .element: class="fragment inlineBlock" -->
		* Si es `long`: Con sufijo `l` o `L`
 		
		<!-- .element: class="fragment inlineBlock" -->
		* Si no lo haces &rArr; ERROR 
		
		<!-- .element: class="fragment inlineBlock" -->

```csharp
// Ejemplos
int entero;
long enteroLargo;
float flotante;

entero = 5;
enteroLargo = 5000L;
flotante = 3.1415F;

// Pero si hacemos ... 
flotante = 5.3;
// -> ERROR:  Literal of type double cannot be implicitly 
// converted to type `float'. Add suffix `f' to create a 
// literal of this type

entero = 5.3F; 
// -> ERROR: Constant value `5.3' cannot be converted to a `int'

// Al revés no hay problema ¿Por qué?
flotante = 2;

```
<!-- .element: class="fragment" style="position:absolute; bottom:7%; right:5px; font-size:15px;" -->


Ver [Built-In Types Table (C# Reference)](https://msdn.microsoft.com/en-us/library/ya5y69ds.aspx)

<!-- .element: class="fragment right smaller" -->

----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

#### Arrays y Colecciones
<!-- .element: class="noVmargins" --> 

* No son como los de Javascript

	* Todos los elementos han de ser del mismo tipo y se declara el tipo en el momento en el que se define la variable
	
	```csharp
	string[] alumnos;
	```
	<!-- .element: class="noVmargins"  style="font-size:18px"-->
	* Hay que crear el *objeto* array para asignárselo  
	
	```csharp
	alumnos = new string[27];
	```
	<!-- .element: class="noVmargins" style="font-size:18px"-->  
	
	* Son de longitud fija. Una vez creados no se puede cambiar su longitud
	```csharp
	alumnos[27] = "Pepito Grillo"; // Si no existía el elemento 27 en alumnos ---> ERROR: Index Out of Range
	```
	<!-- .element: class="noVmargins" style="font-size:18px"-->  
	

* Colecciones

	* `List<Type>`

	* `Dictionary<Type1, Type2>`	





----

Variables en C#
<!-- .element: class="header" -->

### Tipos de Unity
El evidente:  

`GameObject`  <!-- .element: class="formula short light" -->  
Además, cada componente tiene su tipo:  

`Transform`  <!-- .element: class="formula short light" -->
`Collider`  <!-- .element: class="formula short light" -->
`Rigidbody`  <!-- .element: class="formula short light" -->
`Renderer`  <!-- .element: class="formula short light" -->
`Light`  <!-- .element: class="formula short light" -->
`Animator`  <!-- .element: class="formula short light" -->
`Animation`  <!-- .element: class="formula short light" -->
...  
También hay otros que son útiles:

`Vector3`  <!-- .element: class="formula short light" -->
`Quaternion`  <!-- .element: class="formula short light" -->
`Random`  <!-- .element: class="formula short light" -->
`Mathf`  <!-- .element: class="formula short light" -->
...


-----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->


#### Uso de una variable

**Lectura:**  
Al ejecutarse el programa, cada vez que aparezca el nómbre de una variable, se sustutirá por el valor que tenga asignado la variable en ese momento.

<!-- .element: class="small" -->


**Escritura:**  
Cada vez que asignamos un valor a una variable mediante el operador `=`, estamos escribiendo o sobrescribiendo su valor.

<!-- .element: class="small" -->

```csharp
string nombre;
string apellidos;
int edad;

// Al asignar valores estamos escribiendo en las variables
nombre = "Pepito";
apellidos = "Grillo Pérez";
edad = 23;

// Leemos los valores usando el nombre de las variables y utilizamos 
// el operador concatenación para construir un mensaje de bienvenida
string saludo = "Hola, " + nombre + " " + apellidos + 
	". Me han dicho que tienes " + edad + "años";
```
<!-- .element: class="comparison" -->

```csharp
// Al enviar el valor de saludo a la consola podemos ver que los 
// valores han sustituido a las variables correctamente:
Debug.Log(saludo);

// Podemos cambiar el valor de las variables volviendo a asignarles
// otro valor
edad = 35;


saludo = "Perdona, " + nombre + 
	". Me acaban de decir que en realidad tienes " + edad + "años";
// Al volver a usar ese nombre de variable se sustiruirá por el 
// valor actualizado
```
<!-- .element: class="comparison" -->


-----



Variables en C#
<!-- .element: class="header" -->


#### ¿Qué significa asignar?

* Asignar es darle valor a una variable. Para eso se utiliza el operador de *asignación*: `=`.

  <!-- .element: class="fragment" -->
```csharp
float numero;
//...
numero = 3.4f;			
```
<!-- .element: class="fragment" -->

* El operador `=` asigna el valor que hay a su derecha (`3.4f`) a la variable que hay a su izquierda (`numero`).

<!-- .element: class="fragment" -->
<br>
* En C#, si el tipo del valor que hay a la derecha no coincide con el tipo de la variable que hay a la izquierda del `=` se produce un error al intentar compilar el programa.
  
<!-- .element: class="fragment" -->
```csharp
numero = "tres coma cuatro";	-> ERROR: Cannot implicitly convert type `string' to `int'		
```
<!-- .element: class="fragment" -->

* Eso es bueno, porque el compilador nos está avisando de que hay algo que está realmente mal en nuestro programa antes de que se pueda ejecutar.
<!-- .element: class="fragment" -->

----


<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

#### No siempre tenemos la información que necesitamos

Cuando escribimos el programa no sabemos que va a hacer el jugador exactamente, que datos vamos a recibir de internet o del disco duro.

* No siempre sabemos que valor concreto hay que asignarle a una variable en el momento de escribir el programa. 



Pero sí que sabremos de dónde y cómo hay que obtenerlo o calcularlo.

* Tanto C# como Unity poseen funciones pre-programadas que nos pueden devolver el valor que necesitamos. <br>
	```csharp
	numero = Transform.Distance(transform.position, playerTr.position);
	```


* Si no existe la función que necesitamos la crearemos nosotros.


----


Variables en C#
<!-- .element: class="header" -->

*No siempre tenemos la información que necesitamos*

<!-- .element: class="right small" -->

* Si no existe la función que necesitamos la crearemos nosotros.  

```csharp
float strength = 5.5F;
float weaponMultiplier = 1.2F;
float health = 8.3F;
float maxHealt = 10F;

//...
private float getAttackValue() {
	return (strength - health/maxHealth) * weaponMultiplier;
}

private void attack(target) {
	//...
	damage = getAttackValue() - enemy.getResistanceValue();
	//...
}
```
<!-- .element: class="fragment"  -->

**Importante:** el tipo del valor que devuelve el operador `-` al actuar sobre los valores que devuelven `getAttackValue()` y `enemy.getResistanceValue()` debe ser `float`. Para que esto sea así ambas funciones deben devolver un `float`.

<!-- .element: class="fragment left small" contenteditable="true"-->

-----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

### No olvidéis

* El tipo de una variable al definirla y al asignarle valor.

<!-- .element: class="fragment" -->
* Que en C# no se usa la palabra clave `var`.

<!-- .element: class="fragment" -->
* Poner nombres significativos y descriptivos a las variables

<!-- .element: class="fragment" -->
* No usar caracteres acentuados, ni ñ, ni símbolos extraños...

<!-- .element: class="fragment" -->
* Utilizar un criterio uniforme para las mayúsculas y minúsculas:
<!-- .element: class="fragment" -->
	* *camelCase* para las variables y funciones  
	
	<!-- .element: class="fragment" -->
	* *PascalCase* para las clases  

	<!-- .element: class="fragment" -->
	* Todo mayúsculas para las constantes  

	<!-- .element: class="fragment" -->
	
	
----

<!-- .slide: class="left" -->

Variables en C#
<!-- .element: class="header" -->

*No olvidéis*

<!-- .element: class="right small" -->

* Que el valor que asignamos a una variable puede ser:

	* Un valor literal (`5.3F`, `"Pepito Grillo"`, etc)
	
	<!-- .element: class="fragment" -->
	* El resultado de una expresión con operadores  `nombreCompleto = nombre + apellidos;`
	
	<!-- .element: class="fragment" -->
	
	* El valor que devuelve una función `damage = calculateDamage()`

	<!-- .element: class="fragment" -->
	
* Pero el tipo de lo que hay a la izquierda y lo que hay a la derecha del `=` debe coincidir siempre.
<!-- .element: class="fragment" -->

