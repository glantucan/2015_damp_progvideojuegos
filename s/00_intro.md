<!-- .slide: class="center" -->

# Programación de videojuegos en Unity con C*#*


-----

## Qué es Unity

* Motor de video juegos
  * Motor de render 2D/3D en tiempo real
  * Motor de Física (PhysX y Box2D)
  * Un Framework de programación basado en componentes
  	* (Casi) todo es un GameObject o un componente de un GameObject
  * Un framework de desarrollo multiplataforma. 
  	* Escribes código en C#, UnityScript o Boo. Precompilas al lenguaje nativo de la plataforma			(Java, Objective-C, Javascript, etc.)
  * ...	
* Herramienta de integración: El Editor de Unity
	* Importa modelos 3d, imágenes, sonidos, etc. en un montón de formatos  
    	*  Puedes usarlos como objetos del juego inmediatamente, sin teclear una línea de código
	* Iluminación estática y dinámica
	* Herramienta de animación
	* Máquinas de estados de animación (Mecanim)
	* Configuración WYSIWYG de todos los aspectos visuales de las escenas de juego
		* Muy bueno para prototipar mecanicas de juego
	