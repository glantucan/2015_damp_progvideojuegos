﻿using UnityEngine;
using System.Collections;

public class PaddleBounceControl : MonoBehaviour {
    private Collider2D collider;
    private float semiWidth;
	// Use this for initialization
	void Start () {
        collider = GetComponent<Collider2D>();
        semiWidth = collider.bounds.size.x / 2;
    }
	
	void OnCollisionEnter2D(Collision2D col) {
        var contactPosX = col.contacts[0].point.x;
        var paddlePosX = contactPosX - transform.position.x;
        col.rigidbody.velocity += Vector2.right * (paddlePosX / semiWidth) * 2;
    }
}
