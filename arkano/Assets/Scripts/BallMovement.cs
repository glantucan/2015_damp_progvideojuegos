﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {

    public float vel;
    private PaddleMovement paddleMov;
    private Vector3 paddleBallV3;
    private bool ballFree;
    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        // We  use the PaddelMovement component to find the ball
        paddleMov = GameObject.FindObjectOfType<PaddleMovement>();
        rb = GetComponent<Rigidbody2D>();
        // As all components have a reference to the transform of their GameObjects 
        // we can use it to get the position
        paddleBallV3 = transform.position - paddleMov.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (!ballFree) {
            transform.position = paddleMov.transform.position + paddleBallV3;
            if (Input.GetMouseButtonDown(0)) {
                ballFree = true;
                rb.velocity = new Vector2(Random.Range(-vel, vel), vel);
            }
        } 
	}
}
