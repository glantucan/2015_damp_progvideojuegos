﻿using UnityEngine;
using System.Collections;

public class DieOnTriggerEnter : MonoBehaviour {

    private LevelManager levelMgr;
    
    void Start() {
        levelMgr = GameObject.FindObjectOfType<LevelManager>();
    }
	
    void OnTriggerEnter2D(Collider2D col) {
        print("DieOnTrigger Triggered by: " + col.name);
        levelMgr.LoadLevel("Start Menu");
    }
}
