﻿using UnityEngine;
using System.Collections;

public class BrickResistance : MonoBehaviour {

    public int maxHits;
    public Sprite[] hitSprites;
    public bool unbreakable;
    private int hitsCount;
    private LevelManager lvlMgr;
    private SpriteRenderer renderer;



    void Start() {
        if (!unbreakable) {
            lvlMgr = GameObject.FindObjectOfType<LevelManager>();
            lvlMgr.addBrick();
            renderer = GetComponent<SpriteRenderer>();
        }
    }

    void OnCollisionEnter2D (Collision2D col) {
        if (!unbreakable) {
            hitsCount++;
            if (hitsCount >= maxHits) {
                lvlMgr.substractBrick();
                Destroy(gameObject);
            } else {
                loadSprites();
            }
        }
    }

    void loadSprites() {
        int spriteIndex = hitsCount-1;
        renderer.sprite = hitSprites[spriteIndex];


    }
}
